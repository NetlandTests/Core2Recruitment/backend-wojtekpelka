﻿using SIENN.Services.DtoModels;
using SIENN.Services.Models;

namespace SIENN.WebApi.Mappers
{
	public static class CategoryMapper
    {
		public static Category MapToCategory(CategoryDto categoryDto)
		{
			return new Category
			{
				Code = categoryDto.Code,
				Description = categoryDto.Description
			};
		}

		public static CategoryDto MapToDto(Category category)
		{
			return new CategoryDto
			{
				Code = category.Code,
				Description = category.Description
			};
		}

	}
}

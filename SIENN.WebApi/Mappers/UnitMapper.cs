﻿using SIENN.Services.DtoModels;
using SIENN.Services.Models;

namespace SIENN.WebApi.Mappers
{
	public static class UnitMapper
    {
		public static Unit MapToUnit(UnitDto unit)
		{
			return new Unit
			{
				Code = unit.Code,
				Description = unit.Description
			};
		}

		public static UnitDto MapToDto(Unit unit)
		{
			return new UnitDto
			{
				Code = unit.Code,
				Description = unit.Description
			};
		}

	}
}

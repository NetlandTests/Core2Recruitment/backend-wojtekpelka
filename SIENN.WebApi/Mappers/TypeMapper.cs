﻿using SIENN.Services.DtoModels;
using Type = SIENN.Services.Models.Type;

namespace SIENN.WebApi.Mappers
{
	public class TypeMapper
    {
		public static Type MapToType(TypeDto type)
		{
			return new Type
			{
				Code = type.Code,
				Description = type.Description
			};
		}

		public static TypeDto MapToDto(Type type)
		{
			return new TypeDto
			{
				Code = type.Code,
				Description = type.Description
			};
		}

	}
}

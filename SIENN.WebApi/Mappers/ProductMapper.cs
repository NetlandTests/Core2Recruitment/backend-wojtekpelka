﻿using SIENN.Services.DtoModels;
using SIENN.Services.Models;
using SIENN.WebApi.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace SIENN.WebApi.Mappers
{
	public static class ProductMapper
    {
		public static Product MaptToProduct(ProductDto productDto)
		{
			return new Product()
			{
				Code = productDto.Code,
				Description = productDto.Description,
				Price = productDto.Price,
				DeliveryDate = productDto.DeliveryDate,
				IsAvailable = productDto.IsAvailable,
				TypeCode = productDto.TypeCode,
				UnitCode = productDto.UnitCode,
				ProductCategories = productDto.ProductCategoryCodes?.Select(x => new ProductCategory
				{
					CategoryCode = x,
					ProductCode = productDto.Code
				}).ToArray()

			};
		}

		public static ProductDto MapToDto(Product product)
		{
			return new ProductDto
			{
				Code = product.Code,
				Description = product.Description,
				DeliveryDate = product.DeliveryDate,
				IsAvailable = product.IsAvailable,
				Price = product.Price,
				TypeCode = product.TypeCode,
				UnitCode = product.UnitCode,
				ProductCategoryCodes = product.ProductCategories?.Select(x => x.CategoryCode).ToArray()
			};
		}

		public static List<ProductDto> MapCollectionToDto(IEnumerable<Product> products)
		{
			var dtoList = new List<ProductDto>();

			foreach (var product in products)
			{
				dtoList.Add(MapToDto(product));
			}

			return dtoList;
		}

		public static FormatedProductViewModel MapToFormatedProduct(Product product)
		{
			var isAvailable = product.IsAvailable ? "Dostępny" : "Niedostępny";
			return new FormatedProductViewModel()
			{
				Description = $"({product.Code}){product.Description}",
				CategoriesCount = product.ProductCategories.Count,
				Price = product.Price,
				DeliveryDate = $"{product.DeliveryDate.Value.Date}",
				IsAvailable = isAvailable,
				Type = $"({product.TypeCode}){product.Type.Description}",
				Unit = $"({product.UnitCode}){product.Unit.Description}"
			};
		}
	}
}

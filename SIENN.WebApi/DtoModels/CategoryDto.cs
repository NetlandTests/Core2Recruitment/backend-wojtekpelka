﻿namespace SIENN.Services.DtoModels
{
	public class CategoryDto
    {
		public string Code { get; set; }
		public string Description { get; set; }
	}
}

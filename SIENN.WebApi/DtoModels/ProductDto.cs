﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SIENN.Services.DtoModels
{
	public class ProductDto
    {
		[Required]
		public string Code { get; set; }

		[MaxLength(150)]
		public string Description { get; set; }
		public decimal Price { get; set; }
		public bool IsAvailable { get; set; }
		public DateTime? DeliveryDate { get; set; }
		public string TypeCode { get; set; }
		public string UnitCode { get; set; }

		public ICollection<string> ProductCategoryCodes { get; set; }

	}
}
﻿using System;

namespace SIENN.WebApi.ViewModels
{
	public class FormatedProductViewModel
	{
		public string Description { get; set; }
		public decimal Price { get; set; }
		public string IsAvailable { get; set; }
		public string DeliveryDate { get; set; }
		public int CategoriesCount { get; set; }
		public string Type { get; set; }
		public string Unit { get; set; }
    }
}

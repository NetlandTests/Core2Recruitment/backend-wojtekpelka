﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SIENN.DbAccess.Repositories;
using SIENN.Services.DtoModels;
using SIENN.WebApi.Mappers;
using System;
using System.Net;
using System.Net;

namespace SIENN.WebApi.Controllers
{
	[Route("api/[controller]")]
    public class CategoryController : Controller
    {
		private IRepository _repository;
		private ILogger<CategoryController> _logger;

		public CategoryController(IRepository repository, ILogger<CategoryController> logger)
		{
			_repository = repository;
			_logger = logger;
		}

		[HttpGet("GetCategory")]
		public IActionResult GetCategory(string code)
		{
			try
			{
				var category = _repository.GetCategory(code);

				if (category == null) return NotFound();

				return Ok(CategoryMapper.MapToDto(category));
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message);
				return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		[HttpPost("CreateCategory")]
		public IActionResult CreateCategory(CategoryDto category)
		{
			if (ModelState.IsValid)
			{
				try
				{
					_repository.CreateCategory(CategoryMapper.MapToCategory(category));

					return Ok();
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, ex.Message);
					return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
				}
			}
			else
				return BadRequest();
		}

		[HttpPut("EditCategory")]
		public IActionResult EditCategory(CategoryDto category)
		{
			try
			{
				var editCategory = _repository.GetCategory(category.Code);

				if (editCategory == null) return NotFound();

				_repository.EditCategory(CategoryMapper.MapToCategory(category));

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message);
				return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		[HttpDelete("DeleteCategory")]
		public IActionResult DeleteCategory(string code)
		{
			try
			{
				var categoryToDelete = _repository.GetCategory(code);

				if (categoryToDelete == null) return NotFound();

				_repository.DeleteCategory(categoryToDelete);

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message);
				return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
			}
		}
	}
}

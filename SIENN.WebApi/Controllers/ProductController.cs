﻿using Microsoft.AspNetCore.Mvc;
using SIENN.DbAccess.Repositories;
using SIENN.Services.DtoModels;
using SIENN.WebApi.Mappers;
using System;
using System.Net;
using PagedList;
using Microsoft.Extensions.Logging;

namespace SIENN.WebApi.Controllers
{
	public class ProductController : Controller
    {
		private IRepository _repository;
		private ILogger<ProductController> _logger;

		public ProductController(IRepository productRepository, ILogger<ProductController> logger)
		{
			_repository = productRepository;
			_logger = logger;
		}

		[HttpGet("GetProduct")]
		public IActionResult GetProduct(string code)
		{
			try
			{
				var product = _repository.GetProduct(code);

				if (product == null) NotFound();

				return Ok(ProductMapper.MapToDto(product));
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message);
				return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		[HttpPost("CreateProduct")]
		public IActionResult CreateProduct(ProductDto product)
		{
			if (ModelState.IsValid)
			{
				try
				{
					_repository.CreateProduct(ProductMapper.MaptToProduct(product));

					return Ok();
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, ex.Message);
					return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
				}
			}
			else
				return BadRequest();
		}

		[HttpPut("EditProduct")]
		public IActionResult EditProdcut(ProductDto product)
		{
			try
			{
				var editProduct = _repository.GetProduct(product.Code);

				if (editProduct == null) return NotFound();

				_repository.EditProduct(ProductMapper.MaptToProduct(product));

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message);
				return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		[HttpDelete("DeleteProduct")]
		public IActionResult DeleteProduct(string code)
		{
			try
			{
				var productToDelete = _repository.GetProduct(code);

				if (productToDelete == null) return NotFound();

				_repository.DeleteProduct((productToDelete));

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message);
				return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		[HttpGet("GetAvailible")]
		public IActionResult GetAvailible(int pageNumber, int pageSize)
		{
			try
			{
				var productsDto = ProductMapper.MapCollectionToDto(_repository.GetAllAvalibleProducts());

				var pagedList = new PagedList<ProductDto>(productsDto, pageNumber, pageSize);

				return Ok(pagedList);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message);
				return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		[HttpGet("GetFiltered")]
		public IActionResult GetFiltered(int pageNumber, int pageSize, string categoryCode, string unitCode, string typeCode)
		{
			try
			{
				var productsDto = ProductMapper.MapCollectionToDto(_repository.GetProductsByFilter(categoryCode, unitCode, typeCode));

				var pagedList = new PagedList<ProductDto>(productsDto, pageNumber, pageSize);

				return Ok(pagedList);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message);
				return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		[HttpGet("GetFormated")]
		public IActionResult GetFormated(string code)
		{
			try
			{
				var product = _repository.GetProduct(code);

				if (product == null) return NotFound();

				return Ok(ProductMapper.MapToFormatedProduct(product));
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message);
				return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message); throw;
			}
		}
	}
}
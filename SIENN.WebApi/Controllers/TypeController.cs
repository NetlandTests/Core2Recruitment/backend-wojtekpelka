﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SIENN.DbAccess.Repositories;
using SIENN.Services.DtoModels;
using SIENN.WebApi.Mappers;
using System;
using System.Net;

namespace SIENN.WebApi.Controllers
{
	public class TypeController : Controller
    {
		private IRepository _repository;
		private ILogger<TypeController> _logger;

		public TypeController(IRepository repository, ILogger<TypeController> logger)
		{
			_repository = repository;
			_logger = logger;
		}

		[HttpGet("GetType")]
		public IActionResult GetType(string code)
		{
			try
			{
				var type = _repository.GetType(code);

				if (type == null) NotFound();

				return Ok(TypeMapper.MapToDto(type));
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message);
				return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		[HttpPost("CreateType")]
		public IActionResult CreateType(TypeDto type)
		{
			if (ModelState.IsValid)
			{
				try
				{
					_repository.CreateType(TypeMapper.MapToType(type));

					return Ok();
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, ex.Message);
					return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
				}
			}
			else
				return BadRequest();
		}

		[HttpPut("EditType")]
		public IActionResult EditType(TypeDto type)
		{
			try
			{
				var editType = _repository.GetType(type.Code);

				if (editType == null) return NotFound();

				_repository.EditType(TypeMapper.MapToType(type));

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message);
				return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		[HttpDelete("DeleteType")]
		public IActionResult DeleteType(string code)
		{
			try
			{
				var typeToDelete = _repository.GetType(code);

				if (typeToDelete == null) return NotFound();

				_repository.DeleteType(typeToDelete);

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message);
				return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
			}
		}
	}
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SIENN.DbAccess.Repositories;
using SIENN.Services.DtoModels;
using SIENN.WebApi.Mappers;
using System;
using System.Net;

namespace SIENN.WebApi.Controllers
{
	public class UnitController : Controller
    {
		private IRepository _repository;
		private ILogger<UnitController> _logger;

		public UnitController(IRepository repository, ILogger<UnitController> logger)
		{
			_repository = repository;
			_logger = logger;
		}

		[HttpGet("GetUnit")]
		public IActionResult GetUnit(string code)
		{
			try
			{
				var unit = _repository.GetUnit(code);

				if (unit == null) NotFound();

				return Ok(UnitMapper.MapToDto(unit));
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message);
				return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		[HttpPost("CreateUnit")]
		public IActionResult CreateUnit(UnitDto unit)
		{
			if (ModelState.IsValid)
			{
				try
				{
					_repository.CreateUnit(UnitMapper.MapToUnit(unit));

					return Ok();
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, ex.Message);
					return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
				}
			}
			else
				return BadRequest();
		}

		[HttpPut("EditUnit")]
		public IActionResult EditUnit(UnitDto unit)
		{
			try
			{
				var editUnit = _repository.GetUnit(unit.Code);

				if (editUnit == null) return NotFound();

				_repository.EditUnit(UnitMapper.MapToUnit(unit));

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message);
				return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		[HttpDelete("DeleteUnit")]
		public IActionResult DeleteUnit(string code)
		{
			try
			{
				var unitToDelete = _repository.GetUnit(code);

				if (unitToDelete == null) return NotFound();

				_repository.DeleteUnit(unitToDelete);

				return Ok();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, ex.Message);
				return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
			}
		}
	}
}

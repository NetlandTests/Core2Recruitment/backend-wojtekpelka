﻿using Microsoft.EntityFrameworkCore;
using SIENN.Services.Models;
using System.Collections.Generic;
using System.Linq;
using Type = SIENN.Services.Models.Type;

namespace SIENN.DbAccess.Repositories
{
	public class Repository : IRepository
	{
		private SiennDbContext _context;

		public Repository(SiennDbContext context)
		{
			_context = context;
		}

		public Product GetProduct(string productCode)
		{
			return _context.Products
				.Include("ProductCategories")
				.Include("Type")
				.Include("Unit")
				.First(x => x.Code == productCode);
		}

		public void CreateProduct(Product product)
		{
			_context.Add(product);

			SaveChanges();
		}

		public void EditProduct(Product product)
		{
			var oldProduct = _context.Products.First(x => x.Code == product.Code);

			oldProduct.DeliveryDate = product.DeliveryDate;
			oldProduct.Description = product.Description;
			oldProduct.IsAvailable = product.IsAvailable;
			oldProduct.Price = product.Price;
			oldProduct.TypeCode = product.TypeCode;
			oldProduct.UnitCode = product.UnitCode;

			oldProduct.ProductCategories.Clear();
			foreach (var productCategory in product.ProductCategories)
			{

				oldProduct.ProductCategories.Add(productCategory);
			}
			
			
			SaveChanges();
		}

		public void DeleteProduct(Product product)
		{
			_context.Remove(product);
			SaveChanges();
		}

		public IEnumerable<Product> GetAllAvalibleProducts()
		{
			return _context.Products.Where(x => x.IsAvailable == true);
		}

		public IEnumerable<Product> GetProductsByFilter(string categoryCode, string unitCode, string typeCode)
		{
			var products = _context.Products.ToList();
			if (unitCode != null && unitCode.Length > 0)
				foreach (var product in products)
				{
					if(!(product.UnitCode == unitCode.ToUpper()))
					product.UnitCode.ToUpper();
				}

			if (typeCode != null && typeCode.Length > 0)
				foreach (var product in products)
				{
					if(!(product.TypeCode == typeCode.ToUpper()))
					product.TypeCode.ToUpper();
				}

			if (categoryCode != null && categoryCode.Length > 0)
				foreach (var product in products)
				{
					foreach (var item in product.ProductCategories)
					{
						if (!(item.CategoryCode == categoryCode.ToUpper()))
							item.CategoryCode.ToUpper();
					}
				}

			return products;
		}


		//Categories

		public Category GetCategory(string code)
		{
			return _context.Categories.First(x => x.Code == code);
		}

		public void CreateCategory(Category category)
		{
			_context.Categories.Add(category);

			SaveChanges();
		}

		public void EditCategory(Category newCategory)
		{
			var oldCategory = _context.Categories.First(x => x.Code == newCategory.Code);

			oldCategory.Description = newCategory.Description;

			SaveChanges();
		}

		public void DeleteCategory(Category category)
		{
			_context.Remove(category);
			SaveChanges();
		}


		//Types

		public Type GetType(string code)
		{
			return _context.Types.First(x => x.Code == code);
		}

		public void CreateType(Type type)
		{
			_context.Types.Add(type);

			SaveChanges();
		}

		public void EditType(Type newType)
		{
			var oldType = _context.Types.First(x => x.Code == newType.Code);

			oldType.Description = newType.Description;

			SaveChanges();
		}

		public void DeleteType(Type type)
		{
			_context.Remove(type);
			SaveChanges();
		}


		//Units

		public Unit GetUnit(string code)
		{
			return _context.Units.First(x => x.Code == code);
		}

		public void CreateUnit(Unit unit)
		{
			_context.Units.Add(unit);

			SaveChanges();
		}

		public void EditUnit(Unit newUnit)
		{
			var oldUnit = _context.Types.First(x => x.Code == newUnit.Code);

			oldUnit.Description = newUnit.Description;

			SaveChanges();
		}

		public void DeleteUnit(Unit unit)
		{
			_context.Remove(unit);
			SaveChanges();
		}

		private void SaveChanges()
		{
			_context.SaveChanges();
		}
	}
}

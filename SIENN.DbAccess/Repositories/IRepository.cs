﻿using SIENN.Services.Models;
using System.Collections.Generic;
using Type = SIENN.Services.Models.Type;


namespace SIENN.DbAccess.Repositories
{
	public interface IRepository
    {
		Product GetProduct(string code);

		void CreateProduct(Product product);

		void EditProduct(Product product);

		void DeleteProduct(Product product);

		IEnumerable<Product> GetAllAvalibleProducts();

		IEnumerable<Product> GetProductsByFilter(string categoryCode, string unitCode, string typeCode);


		Category GetCategory(string code);

		void CreateCategory(Category category);

		void EditCategory(Category category);

		void DeleteCategory(Category category);


		Type GetType(string code);

		void CreateType(Type type);

		void EditType(Type type);

		void DeleteType(Type type);


		Unit GetUnit(string code);

		void CreateUnit(Unit unit);

		void EditUnit(Unit unit);

		void DeleteUnit(Unit unit);
	}
}

﻿using Microsoft.EntityFrameworkCore;
using SIENN.Services.Models;
using Type = SIENN.Services.Models.Type;

namespace SIENN.DbAccess
{
	public class SiennDbContext : DbContext
	{
		public DbSet<Product> Products { get; set; }
		public DbSet<Category> Categories { get; set; }
		public DbSet<Unit> Units { get; set; }
		public DbSet<Type> Types { get; set; }
		public DbSet<ProductCategory> ProductsCategories { get; set; }

		public SiennDbContext(DbContextOptions options) : base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			builder.Entity<Product>().HasKey(x => x.Code);
			builder.Entity<Product>().Property(x => x.Code).ValueGeneratedNever();
			builder.Entity<Product>().Property(x => x.Code).HasMaxLength(15);
			builder.Entity<Product>().Property(x => x.Description).HasMaxLength(150);

			builder.Entity<Category>().HasKey(x => x.Code);
			builder.Entity<Category>().Property(x => x.Code).ValueGeneratedNever();
			builder.Entity<Category>().Property(x => x.Description).HasMaxLength(50);


			builder.Entity<Unit>().HasKey(x => x.Code);
			builder.Entity<Unit>().Property(x => x.Code).ValueGeneratedNever();
			builder.Entity<Unit>().Property(x => x.Description).HasMaxLength(50);

			builder.Entity<Type>().HasKey(x => x.Code);
			builder.Entity<Type>().Property(x => x.Code).ValueGeneratedNever();
			builder.Entity<Type>().Property(x => x.Description).HasMaxLength(50);

			builder.Entity<ProductCategory>().HasKey(x => new { x.CategoryCode, x.ProductCode });
			builder.Entity<ProductCategory>().HasOne(pc => pc.Product).WithMany(p => p.ProductCategories).HasForeignKey(pc => pc.ProductCode);
			builder.Entity<ProductCategory>().HasOne(x => x.Category).WithMany(x => x.ProductCategories).HasForeignKey(x => x.CategoryCode);

			base.OnModelCreating(builder);
		}
	}
}

﻿namespace SIENN.Services.Models
{
	public class ProductCategory
    {
		public string CategoryCode { get; set; }

		public Category Category { get; set; }

		public string ProductCode { get; set; }

		public Product Product { get; set; }
	}
}

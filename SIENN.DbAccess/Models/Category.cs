﻿using System.Collections.Generic;

namespace SIENN.Services.Models
{
	public class Category
	{
		public string Code { get; set; }
		public string Description { get; set; }

		public ICollection<ProductCategory> ProductCategories { get; set; }
	}
}

﻿using System.Collections.Generic;

namespace SIENN.Services.Models
{
	public class Unit
    {
		public string Code { get; set; }
		public string Description { get; set; }

		public ICollection<Product> Products { get; set; }
    }
}